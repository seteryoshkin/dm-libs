from setuptools import setup, find_packages

setup(
    name='datamall',
    version='0.0.2',
    description='Package that allows to use some extended datamall features',
    packages=find_packages(),
    install_requires=[],
    include_package_data=True
)
